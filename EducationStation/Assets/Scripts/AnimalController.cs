﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalController : MonoBehaviour
{
    public string animal;
    public GameObject Animal;
    public GameObject Player;
    public float Distance_;

    public AudioSource animalNoise;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            animalNoise.Play();
        }
        
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && (Input.GetKey(KeyCode.C)))
        {
            PlayerMovement.Noise = animalNoise;
        }
        if (other.gameObject.CompareTag("Player") && (Input.GetKey(KeyCode.R)))
        {
            //PlayerMovement.Noise = animalNoise;
            animalNoise = PlayerMovement.Noise;
            
            
        }

    }


    // Update is called once per frame
    void Update()
    {
        Distance_ = Vector3.Distance(Animal.transform.position, Player.transform.position);

        if (Input.GetKey(KeyCode.V))
        {

        }
    }
}
