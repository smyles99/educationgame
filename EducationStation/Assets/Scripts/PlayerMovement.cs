﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public static AudioSource Noise;
    public AudioSource frog;
    public AudioSource horse;
    public AudioSource snake;
    public AudioSource duck;
    public AudioSource chicken;
    public AudioSource pig;
    public AudioSource dog;
    public AudioSource cow;
    // Start is called before the first frame update
    void Start()
    {
      
    }
    void OnMouseDown()
    {
        if (CompareTag("chicken"))
        {
            Noise = duck;
        }
        
    }


    // Update is called once per frame
    void Update()
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (Input.GetKey(KeyCode.A))
            rb.transform.Translate(Vector2.left * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.D))
            rb.transform.Translate(Vector2.right * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.W))
            rb.transform.Translate(Vector2.up * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.S))
            rb.transform.Translate(Vector2.down * speed * Time.deltaTime);



        if (Input.GetKey(KeyCode.F))
        {
            Noise.Play();
            Debug.Log(Noise);
        }

        if (Input.GetKey(KeyCode.V))
        {
            Noise.Stop();
           
        }


    }
}
